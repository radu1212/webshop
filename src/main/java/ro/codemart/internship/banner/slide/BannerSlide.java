package ro.codemart.internship.banner.slide;

import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.resource.SharedResourceReference;
import ro.codemart.internship.banner.BannerArea;
import ro.codemart.internship.header.HeaderPanel;

public class BannerSlide extends Panel {
    public BannerSlide(String id) {
        super(id);
        ExternalLink link = new ExternalLink("banner", "#");
        Image image = new Image("banner-img", new SharedResourceReference(BannerArea.class, "../../../../img/banner/banner-img.png"));
        link.add(image);
        add(link);
    }
}
