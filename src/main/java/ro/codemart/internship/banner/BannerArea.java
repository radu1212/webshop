package ro.codemart.internship.banner;

import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.resource.SharedResourceReference;
import ro.codemart.internship.banner.slide.BannerSlide;

public class BannerArea extends Panel {
    public BannerArea(String id) {
        super(id);

       /* ExternalLink next = new ExternalLink("next", "#");
        Image next_slide = new Image("next-slide", new SharedResourceReference(BannerArea.class, "../../../../img/banner/next.png"));
        next.add(next_slide);
        add(next);

        ExternalLink prev = new ExternalLink("prev", "#");
        Image prev_slide = new Image("prev-slide", new SharedResourceReference(BannerArea.class, "../../../../img/banner/prev.png"));
        prev.add(prev_slide);
        add(prev);
*/
        add(new BannerSlide("first-banner"));
        add(new BannerSlide("second-banner"));
    }
}
