package ro.codemart.internship.product.preview.slider;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import ro.codemart.internship.product.preview.singularproduct.ProductComponent;
import ro.codemart.internship.utilities.StringConstants;

public class ProductsSlider extends Panel {

    public ProductsSlider(String id, String productRelease) {
        super(id);
        add(new Label("product-release", productRelease));
        for (int i = 1; i <= 8; i++) {
            add(new ProductComponent(
                    "prod" + i,
                    StringConstants.getBasePathForImagesThreePackagesDeep() + "product/p" + i + ".jpg",
                    StringConstants.getDefaultProductDescription(),
                    "150.00$",
                    "210.00$")
            );
        }
    }

}
