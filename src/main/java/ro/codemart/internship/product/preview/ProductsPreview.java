package ro.codemart.internship.product.preview;

import org.apache.wicket.markup.html.panel.Panel;
import ro.codemart.internship.product.preview.slider.ProductsSlider;

public class ProductsPreview extends Panel {
    public ProductsPreview(String id) {
        super(id);
        add(new ProductsSlider("slider1", "Coming Products"));
        add(new ProductsSlider("slider2", "Latest Products"));
    }
}
