package ro.codemart.internship.product.preview.singularproduct;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import ro.codemart.internship.product.preview.slider.ProductsSlider;
import ro.codemart.internship.utilities.PhotoAdder;

public class ProductComponent extends Panel {
    public ProductComponent(String id) {
        super(id);
    }

    public ProductComponent(String id, String pathToProductImage, String description, String newPrice, String oldPrice) {
        super(id);
        add(PhotoAdder.addPhoto(pathToProductImage, ProductsSlider.class, "product-preview"));
        add(new Label("new-price", newPrice));
        add(new Label("old-price", oldPrice));
        add(new Label("product-description", description));
    }
}
