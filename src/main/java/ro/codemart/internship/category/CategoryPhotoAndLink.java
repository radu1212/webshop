package ro.codemart.internship.category;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.resource.SharedResourceReference;
import ro.codemart.internship.header.HeaderPanel;
import ro.codemart.internship.utilities.PhotoAdder;
import ro.codemart.internship.utilities.StringConstants;

public class CategoryPhotoAndLink extends Panel {
    public CategoryPhotoAndLink(String id) {
        super(id);
    }

    public void addPhoto(int categoryNo) {
        add(PhotoAdder.addPhoto(StringConstants.
                        getBasePathForImagesOnePackageDeep() + "category/c" + categoryNo + ".jpg",
                CategoryPhotoAndLink.class,
                "photo-id"));
        add(PhotoAdder.addLinkOnly("#", "link-id"));/*
        add(PhotoAdder.addLinkIncludingPhoto(StringConstants.
                        getBasePathForImages() + "category/c" + categoryNo + ".jpg",
                "#",
                CategoryPhotoAndLink.class,
                "link-id",
                "photo-id"));*/
    }
}
