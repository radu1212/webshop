package ro.codemart.internship.category;

import org.apache.wicket.markup.html.panel.Panel;

public class Categories extends Panel {
    public Categories(String id) {
        super(id);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        for (int i = 1; i <= 5; i++) {
            CategoryPhotoAndLink categoryPhotoAndLink = new CategoryPhotoAndLink("category" + i);
            categoryPhotoAndLink.addPhoto(i);
            add(categoryPhotoAndLink);
        }
    }
}
