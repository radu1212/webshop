package ro.codemart.internship.utilities;

public class StringConstants {

    public static String getBasePathForImagesOnePackageDeep() { return "../../../../img/"; }
    public static String getBasePathForImagesTwoPackagesDeep() { return "../../../../../img/"; }
    public static String getBasePathForImagesThreePackagesDeep() { return "../../../../../../img/"; }
    public static String getBasePathForImagesFourPackagesDeep() { return "../../../../../../../img/"; }
    public static String getDefaultProductDescription() { return "addidas New Hammer sole for Sports person"; }
}
