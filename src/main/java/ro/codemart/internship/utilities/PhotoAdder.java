package ro.codemart.internship.utilities;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.link.PopupSettings;
import org.apache.wicket.request.resource.SharedResourceReference;
import ro.codemart.internship.header.HeaderPanel;

public class PhotoAdder {

    public static Image addPhoto(String path, Class<?> className,String imageId){
        return new Image(imageId, new SharedResourceReference(className, path));
    }

    public static ExternalLink addLinkIncludingPhoto(String path, String link, Class<?> className, String aTagId, String imageId){
        ExternalLink aTagLink = new ExternalLink(aTagId, link);
        Image image = new Image(imageId, new SharedResourceReference(className, path));
        aTagLink.add(image);
        return aTagLink;
    }

    public static ExternalLink addLinkOnly(String link, String aTagId){
        return new ExternalLink(aTagId, link);
    }
}
