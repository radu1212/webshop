package ro.codemart.internship;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import ro.codemart.internship.banner.BannerArea;
import ro.codemart.internship.category.Categories;
import ro.codemart.internship.deals.DealsPanel;
import ro.codemart.internship.features.FeaturesArea;
import ro.codemart.internship.header.HeaderPanel;
import ro.codemart.internship.product.preview.ProductsPreview;

public class HomePage extends WebPage {

	private static final long serialVersionUID = 1L;

	/**
	 * Base directory:
	 *
	 * added all styles required, taken from the given template html file
	 * @param response - the resources files sent back to be rendered by wicket
	 */
	@Override
	public void renderHead(IHeaderResponse response) {
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/linearicons.css")));
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/font-awesome.min.css")));
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/themify-icons.css")));
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/bootstrap.css")));
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/owl.carousel.css")));
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/nice-select.css")));
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/nouislider.min.css")));
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/ion.rangeSlider.css")));
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/ion.rangeSlider.skinFlat.css")));
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/magnific-popup.css")));
		response.render(CssHeaderItem.forReference(new CssResourceReference(HomePage.class, "../../../css/main.css")));

		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/vendor/jquery-2.2.4.min.js")));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/vendor/bootstrap.min.js")));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/jquery.ajaxchimp.min.js")));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/jquery.nice-select.min.js")));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/jquery.sticky.js")));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/nouislider.min.js")));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/countdown.js")));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/jquery.magnific-popup.min.js")));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/owl.carousel.min.js")));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/gmaps.min.js")));
		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(HomePage.class, "../../../js/main.js")));
	}

	public HomePage(final PageParameters parameters) {
		super(parameters);

		add(new HeaderPanel("header"));
		add(new BannerArea("banner"));
		add(new FeaturesArea("features"));
		add(new Categories("categories"));
		add(new ProductsPreview("product-preview"));
		add(new DealsPanel("deals-panel"));
		// TODO Add your page's components here

	}
}
