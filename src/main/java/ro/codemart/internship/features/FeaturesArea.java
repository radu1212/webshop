package ro.codemart.internship.features;

import org.apache.wicket.markup.html.panel.Panel;
import ro.codemart.internship.features.feature.SingleFeature;

public class FeaturesArea extends Panel {

    public FeaturesArea(String id) {
        super(id);
        for (int i = 1; i < 5; i++) {
            SingleFeature singleFeature = new SingleFeature("feature" + i);
            singleFeature.addImage("../../../../img/features/f-icon" + i + ".png", "This is feature number " + i );
            add(singleFeature);
        }
    }
}
