package ro.codemart.internship.features.feature;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.resource.SharedResourceReference;
import ro.codemart.internship.header.HeaderPanel;

public class SingleFeature extends Panel {

    public SingleFeature(String id) {
        super(id);
    }

    public void addImage(String pathToImage, String textToBeDisplayed){
        ExternalLink aTagLink = new ExternalLink("linkId", "#");
        Image image = new Image("imagId", new SharedResourceReference(HeaderPanel.class, pathToImage));
        aTagLink.add(image);
        add(aTagLink);
        add(new Label("feature-text", textToBeDisplayed));
    }
}
