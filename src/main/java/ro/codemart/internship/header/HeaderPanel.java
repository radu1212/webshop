package ro.codemart.internship.header;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.resource.ContextRelativeResource;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.request.resource.SharedResourceReference;
import ro.codemart.internship.HomePage;

public class HeaderPanel extends Panel {

    public HeaderPanel(String id) {
        super(id);
        //add(new Image("logo", new ContextRelativeResource("logo.png")));
        ExternalLink link = new ExternalLink("logo", "#");
        Image image = new Image("img", new SharedResourceReference(HeaderPanel.class, "../../../../img/logo.png"));
        link.add(image);
        add(link);
    }

}
