package ro.codemart.internship.deals.products;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import ro.codemart.internship.deals.DealsPanel;
import ro.codemart.internship.utilities.PhotoAdder;
import ro.codemart.internship.utilities.StringConstants;

public class ProductsDeals extends Panel {
    public ProductsDeals(String id, String newPrice, String oldPrice, String productDescription) {
        super(id);
        add(PhotoAdder.addPhoto(StringConstants.getBasePathForImagesOnePackageDeep()+ "product/e-p1.png", DealsPanel.class, "deal-img"));
        add(new Label("old-price", oldPrice));
        add(new Label("new-price", newPrice));
        add(new Label("description", productDescription));
    }
}
