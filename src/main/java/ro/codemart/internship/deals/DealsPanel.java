package ro.codemart.internship.deals;

import org.apache.wicket.markup.html.panel.Panel;
import ro.codemart.internship.deals.products.ProductsDeals;

public class DealsPanel extends Panel {
    public DealsPanel(String id) {
        super(id);
        add(new ProductsDeals("deal1", "150.00$", "210.00$", "addidas New Hammer sole for Sports person"));
        add(new ProductsDeals("deal2", "150.00$", "210.00$", "addidas New Hammer sole for Sports person"));
    }
}
